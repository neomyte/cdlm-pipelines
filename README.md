# CDLM-Pipelines

## Description

This repo contains all the pipelines for the site called Concept De La Muerte.

## Tech stack

* **Groovy**
* **Ansible**
* **Python**
* **Docker**
* **Bash**

## Contributors

* Romain Haas
* Emmanuel Pluot