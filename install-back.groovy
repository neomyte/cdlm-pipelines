def gitlabCredentials = 'gitlab'
def repo = '/data'
def playbookRepo = repo + '/playbooks'

def install(app){
    play """${app}_deploy""", 'inv'
}

pipeliner{
    
    token '4e4412eeae2f8bdeb18ce489da6a78f6'

    stage('Getting installation code'){
        dir(repo){
            git(branch: 'master', credentialsId: gitlabCredentials, url: "https://gitlab.com/neomyte/cdlm-pipelines")
        }
    }

    stage('Installing the application'){
        dir(playbookRepo){
            install 'back'
        }
    }
}