def gitlabCredentials = 'gitlab'
def repo = '/data'
def playbookRepo = repo + '/playbooks'

def install(app){
    play """${app}_deploy""", 'inv'
}

pipeliner{

    token '085462d1789b31ea842f084d10c6c7f9'

    stage('Getting installation code'){
        dir(repo){
            git(branch: 'master', credentialsId: gitlabCredentials, url: "https://gitlab.com/neomyte/cdlm-pipelines")
        }
    }

    stage('Installing the application'){
        dir(playbookRepo){
            install 'front'
        }
    }
}