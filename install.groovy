def gitlabCredentials = 'gitlab'
def repo = '/data'
def playbookRepo = repo + '/playbooks'

def install(app){
    play """${app}_deploy""", 'inv'
}

pipeliner{

    stage('Getting installation code'){
        dir(repo){
            git(branch: 'master', credentialsId: gitlabCredentials, url: "https://gitlab.com/neomyte/cdlm-pipelines")
        }
    }

    stage('Installing the application'){
        dir(playbookRepo){
            parallel(
                'back': {
                    install 'back'
                },
                'front': {
                    install 'front'
                }
            )
        }
    }
}