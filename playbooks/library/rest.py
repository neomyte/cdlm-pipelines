#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# (c) 2019, CGI
#

DOCUMENTATION = '''
---
short_description: Call back's API

description:

module: rest

version_added: "1"

rest:
    url:
    description: URL to attack
    object:
    description: Object to call API for
    crud:
    description: Type of CRUD
    choices: ['create', 'read', 'update', 'delete']
    params:
    description: A dictionary of needed params
    default: {}
    timeout:
    description: A timeout
    default: 10

author: Neomyte
'''

EXAMPLE='''
rest:
    url: http://back:4200
    object: concept
    crud: create
    params:
    name: test
    parents: test_
    timeout: 10
'''

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.urls import *

def main():
    global module
    crud_names = [ 'create', 'read', 'update', 'delete' ]

    args = dict(
        url       = dict(required=True,  type='str'),
        object    = dict(required=True,  type='str'),
        crud      = dict(required=True,  type='str',  choices=crud_names),
        params    = dict(required=False, type='dict', default={}),
        timeout   = dict(required=False, type='int',  default=10)
    )

    module = AnsibleModule(
        argument_spec=args,
    )

    url      = module.params.get('url')
    object   = module.params.get('object')
    crud     = module.params.get('crud')
    params   = module.params.get('params')
    timeout  = module.params.get('timeout')

    ctoh = {
        'create': 'post',
        'read': 'get',
        'update': 'put',
        'delete': 'delete'
    } #crud to http

    tmp = ['/'.join(i) for i in params.items()]
    tmp.sort()
    url = url + '/' + crud + '/' + object + '/' + '/'.join(tmp)

    try:
        result = json.load(open_url(url, method=ctoh[crud], timeout=timeout))
        module.exit_json(changed=result['ansibleState'] == 'CHANGED', failed=result['ansibleState'] == 'FAILURE', facts=result['information'], msg=result['message'])
    except Exception as err:
        module.exit_json(changed=False, failed=True, msg='Error:'+str(err)+'\nFor URL: '+ url + '\n')

if __name__ == "__main__":
    main()